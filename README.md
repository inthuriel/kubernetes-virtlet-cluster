# Kubernetes stack with _Mirantis_ virtlet

This _Terraform_ stack creates _Kubernetes_ cluster in _Openstack_ with preinstalled _Mirantis_ virtlet software.  
You may shape parameters of the cluster available in **variables.tf** file.  

## Requirements
This resources are required to successfully run cluster:  

 * _Openstack_ **fip**: you have to ensure available _fip_ for each server in cluster (both _master_ and _worker_ nodes)  
 * _apt-based_ image for _vms_: this cluster was tested on **Ubuntu 16.04**  
 * _Openstack_ _external-network_: alongside with _fips_ you have to ensure _external-network_  
 * internet access: all _vms_ should have to access internet with _DNS_  
 * **Terraform** version **>=0.11.10**  
 * **Openstack** version **>=Ocata**  

## Known limitations
### Cluster hosts

1. cluster has to have at least **one** master  
2. master shouldn't be deleted as it's used as _seed_ for cluster  
3. workloads (including **virtlet**) are spawned only on _worker_ nodes

### Cluster keys
All hosts in cluster uses keys stored in **_resources** folder in this repository:  

* __id_rsa__ - as private key  
* __id_rsa.pub__ - as public keys  

Those **file names and path are hardcoded**. You may replace those keys with your own, but you have to preserve file names and location.  

### OpenStack auth
This setup uses _openstack provider_:  
```
provider "openstack" {

}
```

you have to set all necessary _OpenStack_ variables eg. by sourcing **openrc** file corresponding with yours _OpenStack_ Tennant  

### Software versions
Used _virtlet_ version **1.1.2** requires _Kubernetes_ **1.10.x** and _Kubernetes_ should work with _Docker_ **17.03.x**  

 ## TODO  

 1. increase etcd to master nodes number  
 2. increase kube-scheduler to master nodes number    
 3. re-trigger / trigger states on node add
 4. add github.com/kubernetes-incubator/cri-tools/cmd/crict
