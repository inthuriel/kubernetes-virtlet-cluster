#cloud-config
package_upgrade: false
users:
  - name: ${user}
    sudo: ALL=(ALL) NOPASSWD:ALL
    groups: sudo, users, admin
    ssh-import-id: None
    home: /home/${user}
    shell: /bin/bash
    lock_passwd: true
    ssh-authorized-keys:
      - ${pub_key}
runcmd:
  - mkdir -p /home/${user}/.ssh
  - chown ${user}:${user} /home/${user}/.ssh
