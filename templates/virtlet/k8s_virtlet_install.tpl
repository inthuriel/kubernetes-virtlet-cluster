#!/bin/bash
curl -SL -o /tmp/virtlet_images.yaml https://raw.githubusercontent.com/Mirantis/virtlet/master/deploy/images.yaml
kubectl create configmap -n kube-system virtlet-image-translations --from-file /tmp/virtlet_images.yaml
rm -f /tmp/virtlet_images.yaml
curl -SL -o /tmp/virtletctl https://github.com/Mirantis/virtlet/releases/download/v1.1.2/virtletctl
sudo mv /tmp/virtletctl /usr/bin/
sudo chmod +x /usr/bin/virtletctl
virtletctl gen | kubectl apply -f -
