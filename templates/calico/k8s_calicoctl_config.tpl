apiVersion: v1
kind: calicoApiConfig
metadata:
spec:
  datastoreType: "kubernetes"
  kubeconfig: "/home/${user}/.kube/config"
