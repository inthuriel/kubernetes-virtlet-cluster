variable "os_image" {
  default = "ubuntu-16.04"
}

variable "master_flavor" {
  default = "m1.small"
}

variable "worker_flavor" {
  default = "m1.small"
}

variable "docker_version" {
  default = "17.03.3~ce-0~ubuntu-xenial"
}

variable "kubernetes_version" {
  default = "1.10.11-00"
}

variable "k8s_master_nubmer" {
  default = 3
}

variable "k8s_worker_nubmer" {
  default = 3
}

variable "default_user" {
  default = "ubuntu"
}

variable "k8s_control_plane_cidr" {
  default = "10.0.50.0/24"
}

variable "k8s_data_plane_cidr" {
  default = "10.0.60.0/24"
}

variable "public_network" {
  default = "public"
}
