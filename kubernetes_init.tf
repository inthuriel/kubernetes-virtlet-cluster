data "template_file" "k8s-base-install" {
  template = "${file("templates/kubernetes/k8s_base_install.tpl")}"
  vars {
      docker_version      = "${var.docker_version}"
      kubernetes_version  = "${var.kubernetes_version}"
  }
}

resource "null_resource" "k8s-master-base-setup" {
  count       = "${var.k8s_master_nubmer}"
  depends_on  = ["null_resource.k8s-master-system-setup"]

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-master-fip.*.address, count.index)}"
    }

    content     = "${data.template_file.k8s-base-install.rendered}"
    destination = "/tmp/k8s_install.sh"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-master-fip.*.address, count.index)}"
    }

    inline = [
      "sudo sh -c \"echo '${format("%s %s %s", element(openstack_compute_floatingip_v2.k8s-master-fip.*.address, count.index), element(openstack_compute_instance_v2.k8s-master.*.access_ip_v4, count.index), element(openstack_compute_instance_v2.k8s-master.*.name, count.index))}' >> /etc/hosts\"",
      "sudo chmod +x /tmp/k8s*",
      "sudo /tmp/k8s_install.sh",
    ]
  }
}

resource "null_resource" "k8s-worker-base-setup" {
  count       = "${var.k8s_worker_nubmer}"
  depends_on  = ["null_resource.k8s-worker-system-setup"]

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-worker-fip.*.address, count.index)}"
    }

    content     = "${data.template_file.k8s-base-install.rendered}"
    destination = "/tmp/k8s_install.sh"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-worker-fip.*.address, count.index)}"
    }

    inline = [
      "sudo sh -c \"echo '${format("%s %s %s", element(openstack_compute_floatingip_v2.k8s-worker-fip.*.address, count.index), element(openstack_compute_instance_v2.k8s-worker.*.access_ip_v4, count.index), element(openstack_compute_instance_v2.k8s-worker.*.name, count.index))}' >> /etc/hosts\"",
      "sudo chmod +x /tmp/k8s*",
      "sudo /tmp/k8s_install.sh",
    ]
  }
}
