#!/bin/bash
kubeadm init --pod-network-cidr=${cidr} --apiserver-advertise-address=${master}
mkdir -p /home/${default_user}/.kube
cp -i /etc/kubernetes/admin.conf /home/${default_user}/.kube/config
chown ${default_user}:${default_user} /home/${default_user}/.kube/config
kubeadm token create --print-join-command > /home/${default_user}/k8s-connect-string
chown ${default_user}:${default_user} /home/${default_user}/k8s-connect-string
