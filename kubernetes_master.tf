data "template_file" "k8s-master-setup" {
  template = "${file("templates/kubernetes/k8s_master_setup.tpl")}"
  vars {
      cidr         = "${var.k8s_data_plane_cidr}"
      master       = "#master#"
      default_user = "${var.default_user}"
  }
}

data "template_file" "k8s-calico-install" {
  template = "${file("templates/calico/k8s_calico_install.tpl")}"
}

data "template_file" "k8s-calicoctl-config" {
  template = "${file("templates/calico/k8s_calicoctl_config.tpl")}"
  vars {
      user      = "${var.default_user}"
  }
}

data "template_file" "k8s-calicoctl-install" {
  template = "${file("templates/calico/k8s_calicoctl_install.tpl")}"
  vars {
      calicoctl_config      = "${data.template_file.k8s-calicoctl-config.rendered}"
  }
}

data "template_file" "k8s-etcd-install" {
  template = "${file("templates/calico/k8s_etcd_install.tpl")}"
  vars {
    masters = "${var.k8s_master_nubmer}"
    nodes   = "${var.k8s_master_nubmer + var.k8s_worker_nubmer}"
  }
}

resource "null_resource" "k8s-1st-master-setup" {
  depends_on  = ["null_resource.k8s-master-base-setup"]

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${openstack_compute_floatingip_v2.k8s-master-fip.0.address}"
    }

    content     = "${data.template_file.k8s-master-setup.rendered}"
    destination = "/tmp/k8s_master_setup.sh"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${openstack_compute_floatingip_v2.k8s-master-fip.0.address}"
    }

    inline = [
      "sudo chmod +x /tmp/k8s*",
      "sed -i -e 's|#master#|${openstack_compute_instance_v2.k8s-master.0.network.1.fixed_ip_v4}|g' /tmp/k8s_master_setup.sh",
      "sudo sh -c \"echo >> /etc/default/kubelet\"",
      "sudo sh -c \"echo 'Environment=KUBELET_EXTRA_ARGS=\"--node-ip=${openstack_compute_instance_v2.k8s-master.0.network.1.fixed_ip_v4}\"' >> /etc/systemd/system/kubelet.service.d/10-kubeadm.conf\"",
      "sudo systemctl daemon-reload",
      "sudo /tmp/k8s_master_setup.sh",
    ]
  }
}


resource "null_resource" "k8s-masters-setup" {
  count       = "${var.k8s_master_nubmer - 1}"
  depends_on  = ["null_resource.k8s-1st-master-setup"]

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-master-fip.*.address, count.index + 1)}"
    }

    inline = [
      "scp ${openstack_compute_instance_v2.k8s-master.0.network.1.fixed_ip_v4}:k8s-connect-string .",
      "sudo chmod +x ./k8s-connect-string",
      "sudo sh -c \"echo >> /etc/default/kubelet\"",
      "sudo sh -c \"echo 'Environment=KUBELET_EXTRA_ARGS=\"--node-ip=${element(openstack_compute_instance_v2.k8s-master.*.network.1.fixed_ip_v4, count.index + 1)}\"' >> /etc/systemd/system/kubelet.service.d/10-kubeadm.conf\"",
      "sudo systemctl daemon-reload",
      "sudo ./k8s-connect-string",
      "mkdir -p /home/${var.default_user}/.kube",
      "scp ${openstack_compute_instance_v2.k8s-master.0.network.1.fixed_ip_v4}:/home/${var.default_user}/.kube/config /home/${var.default_user}/.kube/config",
      "chown ${var.default_user}:${var.default_user} /home/${var.default_user}/.kube/config",
      "kubectl label nodes ${element(openstack_compute_instance_v2.k8s-master.*.name, count.index + 1)} node-role.kubernetes.io/master=\"\"",
      "kubectl taint nodes ${element(openstack_compute_instance_v2.k8s-master.*.name, count.index + 1)} node-role.kubernetes.io/master=:NoSchedule"
    ]
  }
}

resource "null_resource" "k8s-calico-master" {
  depends_on  = ["null_resource.k8s-masters-setup", "null_resource.k8s-workers-setup"]

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${openstack_compute_floatingip_v2.k8s-master-fip.0.address}"
    }

    content     = "${data.template_file.k8s-calico-install.rendered}"
    destination = "/tmp/k8s_calico_install.sh"
  }

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${openstack_compute_floatingip_v2.k8s-master-fip.0.address}"
    }

    content     = "${data.template_file.k8s-etcd-install.rendered}"
    destination = "/tmp/k8s_etcd_install.sh"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${openstack_compute_floatingip_v2.k8s-master-fip.0.address}"
    }

    inline = [
      "sudo chmod +x /tmp/k8s*",
      "/tmp/k8s_calico_install.sh",
      "/tmp/k8s_etcd_install.sh",
    ]
  }
}

resource "null_resource" "k8s-calicoctl-install" {
  count       = "${var.k8s_master_nubmer}"
  depends_on  = ["null_resource.k8s-calico-master"]

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-master-fip.*.address, count.index)}"
    }

    content     = "${data.template_file.k8s-calicoctl-install.rendered}"
    destination = "/tmp/k8s_calicoct_install.sh"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-master-fip.*.address, count.index)}"
    }

    inline = [
      "sudo chmod +x /tmp/k8s*",
      "/tmp/k8s_calicoct_install.sh",
    ]

  }
}
