locals {
  network_name = "k8s"
}

# public network
data "openstack_networking_network_v2" "public-network" {
  name = "${var.public_network}"
}

# control plane network
resource "openstack_networking_network_v2" "k8s-control-plane-network" {
  name           = "${local.network_name}-control-plane-${random_string.deployment_string.result}"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "k8s-control-plane-subnet" {
  name            = "${local.network_name}-control-plane-subnet-${random_string.deployment_string.result}"
  network_id      = "${openstack_networking_network_v2.k8s-control-plane-network.id}"
  cidr            = "${var.k8s_control_plane_cidr}"
  ip_version      = 4
  dns_nameservers = ["1.1.1.1", "1.0.0.1"]
}

resource "openstack_networking_router_v2" "k8s-control-plane-router" {
  name                = "k8s-control-plane-router-${random_string.deployment_string.result}"
  admin_state_up      = "true"
  external_network_id = "${data.openstack_networking_network_v2.public-network.id}"
}

resource "openstack_networking_router_interface_v2" "k8s-control-plane-router-interface" {
  router_id = "${openstack_networking_router_v2.k8s-control-plane-router.id}"
  subnet_id = "${openstack_networking_subnet_v2.k8s-control-plane-subnet.id}"
}

resource "openstack_compute_floatingip_v2" "k8s-master-fip" {
  pool = "${data.openstack_networking_network_v2.public-network.name}"
  count = "${var.k8s_master_nubmer}"
}

resource "openstack_compute_floatingip_v2" "k8s-worker-fip" {
  pool = "${data.openstack_networking_network_v2.public-network.name}"
  count = "${var.k8s_worker_nubmer}"
}

# data plane network
resource "openstack_networking_network_v2" "k8s-data-plane-network" {
  name           = "${local.network_name}-data-plane-${random_string.deployment_string.result}"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "k8s-data-plane-subnet" {
  name            = "${local.network_name}-data-plane-subnet-${random_string.deployment_string.result}"
  network_id      = "${openstack_networking_network_v2.k8s-data-plane-network.id}"
  cidr            = "${var.k8s_data_plane_cidr}"
  ip_version      = 4
}

# control plane security
resource "openstack_networking_secgroup_v2" "k8s-control-plane-security" {
  name        = "k8s-control-plane-security-${random_string.deployment_string.result}"
  description = "Security group for Kubernetes control traffic"
}

resource "openstack_networking_secgroup_rule_v2" "k8s-22" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.k8s-control-plane-security.id}"
}

resource "openstack_networking_secgroup_rule_v2" "k8s-443" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.k8s-control-plane-security.id}"
}

resource "openstack_networking_secgroup_rule_v2" "k8s-6443" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 6443
  port_range_max    = 6443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.k8s-control-plane-security.id}"
}


# data plane security
resource "openstack_networking_secgroup_v2" "k8s-data-plane-security" {
  name        = "k8s-data-plane-security-${random_string.deployment_string.result}"
  description = "Security group for Kubernetes control traffic"
}

resource "openstack_networking_secgroup_rule_v2" "k8s-allow-internal" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 1
  port_range_max    = 65535
  remote_ip_prefix  = "${var.k8s_data_plane_cidr}"
  security_group_id = "${openstack_networking_secgroup_v2.k8s-data-plane-security.id}"
}

resource "openstack_networking_secgroup_rule_v2" "k8s-ping" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "${var.k8s_data_plane_cidr}"
  security_group_id = "${openstack_networking_secgroup_v2.k8s-data-plane-security.id}"
}
