#!/bin/bash
sudo curl -o /usr/local/bin/calicoctl -L --silent https://github.com/projectcalico/calicoctl/releases/download/v1.6.5/calicoctl
sudo chmod +x /usr/local/bin/calicoctl
sudo mkdir -p /etc/calico
sudo sh -c "echo -n \"${calicoctl_config}\" > /etc/calico/calicoctl.cfg"
