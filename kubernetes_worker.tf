resource "null_resource" "k8s-workers-setup" {
  count       = "${var.k8s_worker_nubmer}"
  depends_on  = ["null_resource.k8s-worker-base-setup", "null_resource.k8s-1st-master-setup", "null_resource.k8s-masters-setup"]

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-worker-fip.*.address, count.index)}"
    }

    inline = [
      "scp ${openstack_compute_instance_v2.k8s-master.0.network.1.fixed_ip_v4}:k8s-connect-string /home/${var.default_user}",
      "sudo chmod +x /home/${var.default_user}/k8s-connect-string",
      "sudo sh -c \"echo >> /etc/default/kubelet\"",
      "sudo sh -c \"echo 'Environment=KUBELET_EXTRA_ARGS=\"--node-ip=${element(openstack_compute_instance_v2.k8s-worker.*.network.1.fixed_ip_v4, count.index)}\"' >> /etc/systemd/system/kubelet.service.d/10-kubeadm.conf\"",
      "sudo systemctl daemon-reload",
      "while [ \"0\" -ge \"$(curl -Ik --silent https://$(cat /home/${var.default_user}/k8s-connect-string | awk '{print $3}') | grep -c 403)\" ]; do echo 'Wait for master API to raise'; sleep 5; done",
      "sudo /home/${var.default_user}/k8s-connect-string",
      "mkdir -p /home/${var.default_user}/.kube",
      "scp ${openstack_compute_instance_v2.k8s-master.0.network.1.fixed_ip_v4}:/home/${var.default_user}/.kube/config /home/${var.default_user}/.kube/config",
      "while [ \"0\" -ge \"$(kubectl get nodes | grep -c ${element(openstack_compute_instance_v2.k8s-worker.*.name, count.index)})\" ]; do echo 'Wait for node to register'; sleep 5; done",
      "kubectl label nodes ${element(openstack_compute_instance_v2.k8s-worker.*.name, count.index)} extraRuntime=virtlet",
    ]
  }
}
