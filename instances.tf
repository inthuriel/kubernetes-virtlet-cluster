resource "openstack_compute_keypair_v2" "default-keypair" {
  name = "k8s-keypair-${random_string.deployment_string.result}"
  public_key = "${file("${path.module}/_resources/id_rsa.pub")}"
}

data "template_file" "ssh-config" {
  template = "${file("${path.module}/templates/os/ssh_config.tpl")}"
  vars {
      user  = "${var.default_user}"
  }
}

data "template_file" "user-data" {
  template = "${file("${path.module}/templates/os/user_data.tpl")}"
  vars {
      user            = "${var.default_user}"
      pub_key         = "${file("${path.module}/_resources/id_rsa.pub")}"
  }
}


resource "openstack_compute_instance_v2" "k8s-master" {
  name            = "k8s-master-${random_string.deployment_string.result}-${count.index}"
  count           = "${var.k8s_master_nubmer}"
  image_name      = "${var.os_image}"
  flavor_name     = "${var.master_flavor}"
  key_pair        = "${openstack_compute_keypair_v2.default-keypair.name}"
  security_groups = [
    "${openstack_networking_secgroup_v2.k8s-control-plane-security.name}",
    "${openstack_networking_secgroup_v2.k8s-data-plane-security.name}"
  ]
  user_data       = "${data.template_file.user-data.rendered}"

  network {
    uuid = "${openstack_networking_network_v2.k8s-control-plane-network.id}"
  }

  network {
    uuid = "${openstack_networking_network_v2.k8s-data-plane-network.id}"
  }
}

resource "openstack_compute_instance_v2" "k8s-worker" {
  name            = "k8s-worker-${random_string.deployment_string.result}-${count.index}"
  count           = "${var.k8s_worker_nubmer}"
  image_name      = "${var.os_image}"
  flavor_name     = "${var.worker_flavor}"
  security_groups = [
    "${openstack_networking_secgroup_v2.k8s-control-plane-security.name}",
    "${openstack_networking_secgroup_v2.k8s-data-plane-security.name}"
  ]
  user_data       = "${data.template_file.user-data.rendered}"

  network {
    uuid = "${openstack_networking_network_v2.k8s-control-plane-network.id}"
  }

  network {
    uuid = "${openstack_networking_network_v2.k8s-data-plane-network.id}"
  }
}

resource "openstack_compute_floatingip_associate_v2" "k8s-master-fip-associate" {
  count       = "${var.k8s_master_nubmer}"
  depends_on  = ["openstack_compute_instance_v2.k8s-master", "openstack_compute_floatingip_v2.k8s-master-fip"]
  floating_ip = "${element(openstack_compute_floatingip_v2.k8s-master-fip.*.address, count.index)}"
  instance_id = "${element(openstack_compute_instance_v2.k8s-master.*.id, count.index)}"
}

resource "openstack_compute_floatingip_associate_v2" "k8s-worker-fip-associate" {
  count       = "${var.k8s_worker_nubmer}"
  depends_on  = ["openstack_compute_instance_v2.k8s-worker", "openstack_compute_floatingip_v2.k8s-worker-fip"]
  floating_ip = "${element(openstack_compute_floatingip_v2.k8s-worker-fip.*.address, count.index)}"
  instance_id = "${element(openstack_compute_instance_v2.k8s-worker.*.id, count.index)}"
}

resource "null_resource" "k8s-master-system-setup" {
  count       = "${var.k8s_master_nubmer}"
  depends_on  = ["openstack_compute_floatingip_associate_v2.k8s-master-fip-associate"]

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-master-fip.*.address, count.index)}"
    }

    content     = "${data.template_file.ssh-config.rendered}"
    destination = "/home/${var.default_user}/.ssh/config"
  }

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-master-fip.*.address, count.index)}"
    }

    source      = "${path.module}/_resources/id_rsa"
    destination = "/home/${var.default_user}/.ssh/k8s-stack-key"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-master-fip.*.address, count.index)}"
    }

    inline = [
      "chmod 0400 /home/${var.default_user}/.ssh/k8s-stack-key"
    ]
  }

}

resource "null_resource" "k8s-worker-system-setup" {
  count       = "${var.k8s_worker_nubmer}"
  depends_on  = ["openstack_compute_floatingip_associate_v2.k8s-worker-fip-associate"]

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-worker-fip.*.address, count.index)}"
    }

    content     = "${data.template_file.ssh-config.rendered}"
    destination = "/home/${var.default_user}/.ssh/config"
  }

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-worker-fip.*.address, count.index)}"
    }

    source      = "${path.module}/_resources/id_rsa"
    destination = "/home/${var.default_user}/.ssh/k8s-stack-key"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-worker-fip.*.address, count.index)}"
    }

    inline = [
      "chmod 0400 /home/${var.default_user}/.ssh/k8s-stack-key"
    ]
  }
}
