#!/bin/bash
run=0; while [ $run -lt 1 ]; do run=`ps aux | grep kubelet | grep -vc grep`; sleep 1; done
run=1; while [ $run -ge 1 ]; do run=`kubectl get pods --all-namespaces | tail -n +2 | awk '{print $4}' | grep -vc Running`; sleep 1; done
curl -SL -o /tmp/criproxy.deb https://github.com/Mirantis/criproxy/releases/download/v0.12.0/criproxy_0.12.0_amd64.deb
sudo systemctl stop kubelet.service
sudo dpkg -i /tmp/criproxy.deb
sudo sh -c "echo > /etc/default/kubelet"
sudo systemctl enable criproxy dockershim
sudo systemctl daemon-reload
sudo systemctl start criproxy dockershim
sudo systemctl restart kubelet.service
sudo rm -f /tmp/criproxy.deb
