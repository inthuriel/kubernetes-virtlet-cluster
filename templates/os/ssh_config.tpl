Host *
  User ${user}
  AddKeysToAgent yes
  StrictHostKeyChecking no
  IdentityFile ~/.ssh/k8s-stack-key
