provider "openstack" {

}

resource "random_string" "deployment_string" {
  length  = 4
  upper   = false
  special = false
  number  = false
}
