data "template_file" "k8s-criproxy-install" {
  template = "${file("templates/virtlet/k8s_cri_proxy_install.tpl")}"
}

data "template_file" "k8s-virtlet-install" {
  template = "${file("templates/virtlet/k8s_virtlet_install.tpl")}"
}

resource "null_resource" "k8s-criproxy-setup" {
  count       = "${var.k8s_worker_nubmer}"
  depends_on  = ["null_resource.k8s-calico-master"]

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-worker-fip.*.address, count.index)}"
    }

    content     = "${data.template_file.k8s-criproxy-install.rendered}"
    destination = "/tmp/k8s_criproxy_install.sh"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-worker-fip.*.address, count.index)}"
    }

    inline = [
      "sudo chmod +x /tmp/k8s*",
      "/tmp/k8s_criproxy_install.sh",
    ]

  }
}

resource "null_resource" "k8s-virtlet-setup" {
  count       = "${var.k8s_worker_nubmer}"
  depends_on  = ["null_resource.k8s-criproxy-setup"]

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-worker-fip.*.address, count.index)}"
    }

    content     = "${data.template_file.k8s-virtlet-install.rendered}"
    destination = "/tmp/k8s_virtlet_install.sh"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "${var.default_user}"
      private_key = "${file("${path.module}/_resources/id_rsa")}"
      host        = "${element(openstack_compute_floatingip_v2.k8s-worker-fip.*.address, count.index)}"
    }

    inline = [
      "sudo chmod +x /tmp/k8s*",
      "/tmp/k8s_virtlet_install.sh",
    ]

  }
}
