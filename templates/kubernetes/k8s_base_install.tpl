#!/bin/bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" >> /etc/apt/sources.list.d/kubernetes.list
echo "deb [arch=amd64] http://apt.kubernetes.io/ kubernetes-$(lsb_release -cs) main" >> /etc/apt/sources.list.d/kubernetes.list
apt-get update -y
apt-get install apt-transport-https ca-certificates curl software-properties-common -y
apt-get update -y
apt-get install docker-ce=${docker_version} -y
apt-get update -y
apt-get install kubelet=${kubernetes_version} kubeadm=${kubernetes_version} kubectl=${kubernetes_version} -y
