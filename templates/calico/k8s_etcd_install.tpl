#!/bin/bash
kubectl apply -f https://docs.projectcalico.org/v3.3/getting-started/kubernetes/installation/hosted/etcd.yaml
while [[ `kubectl get pods --all-namespaces | grep calico-etcd | grep -ic running` -lt ${masters} ]]; do echo 'Wait for etcd cluster' && sleep 2; done;
while [[ `kubectl get pods --all-namespaces | grep calico-node | grep 2/2 | grep -ic running` -lt ${nodes} ]]; do echo 'Wait for callico cluster' && sleep 2; done;
